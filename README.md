# Android Assignment

## Library and Frameworks
1. Minimum SDK level 21
2. [Kotlin](https://kotlinlang.org/) based, [Coroutines](https://github.com/Kotlin/kotlinx.coroutines) + [Flow](https://kotlin.github.io/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines.flow/) for asynchronous.
3. Dagger-Hilt (alpha) for dependency injection.
4. JetPack
    - LiveData - notify domain layer data to views.
    - Lifecycle - dispose of observing data when lifecycle state changes.
    - ViewModel - UI related data holder, lifecycle aware.
5. Architecture
    - MVVM Architecture (View - ViewModel - Model)
    - Repository pattern
6. [Retrofit2 & OkHttp3](https://github.com/square/retrofit) - construct the REST APIs and paging network data.
7. [Retrofit](https://square.github.io/retrofit/) - A type-safe HTTP client for Android and Java.
8. [coil](https://github.com/coil-kt/coil)
9. [Material-Components](https://github.com/material-components/material-components-android) - Material design components
10. Testing
    - [MockK](https://mockk.io/) - Unit testing
    - [JUnit5](https://github.com/mannodermaus/android-junit5)
    - [kotlinx-coroutines-test](https://kotlin.github.io/kotlinx.coroutines/kotlinx-coroutines-test/)
    
## APK file for installation
[Click here](https://bitbucket.org/vipulshah2010/adyenassignment/raw/34ad02887aa0922b7a3ad3fa4902144bffa901b7/art/app-debug.apk) to download apk.

## Demo
![Recording](/art/recording.gif)

## Missing Implementation.
1. Didn't cover instrumentation/UI test cases.
2. Paging is not implemented
3. Filtering is not implemented ( I spent too much time on runtime permission handling & custom search bar :P)
4. Haven't made use of SavedStateHandle provided by ViewModel.
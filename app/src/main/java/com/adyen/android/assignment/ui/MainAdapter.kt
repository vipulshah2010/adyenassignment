package com.adyen.android.assignment.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import coil.transform.CircleCropTransformation
import com.adyen.android.assignment.R
import com.adyen.android.assignment.api.model.RecommendedItem
import com.adyen.android.assignment.databinding.RowVenueBinding

class MainAdapter(private val listener: (RecommendedItem) -> Unit) :
    RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    private var items: List<RecommendedItem> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowVenueBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position], listener)
    }

    fun setItems(items: List<RecommendedItem>) {
        this.items = items
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: RowVenueBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: RecommendedItem, listener: (RecommendedItem) -> Unit) =
            with(itemView) {

                with(item.venue.categories[0]) {
                    binding.typeTextView.text = name
                    binding.imageView.load(icon.prefix + "100" + icon.suffix) {
                        crossfade(true)
                        placeholder(R.drawable.ic_svg_placeholder)
                        background
                        transformations(CircleCropTransformation())
                    }
                }

                binding.locationTextView.text = item.venue.name
                binding.addressTextView.text =
                    item.venue.location.formattedAddress.joinToString("\n")

                setOnClickListener {
                    listener(item)
                }
            }
    }
}
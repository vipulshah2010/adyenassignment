package com.adyen.android.assignment.ui

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.location.Location
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.activity.viewModels
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.adyen.android.assignment.PermissionCallback
import com.adyen.android.assignment.PermissionObserver
import com.adyen.android.assignment.R
import com.adyen.android.assignment.api.model.LatLng
import com.adyen.android.assignment.api.model.VenueResult
import com.adyen.android.assignment.databinding.ActivityMainBinding
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
// Handling runtime permission, so skipping this lint warning.
@SuppressLint("MissingPermission")
class MainActivity : AppCompatActivity(), PermissionCallback {

    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: MainAdapter
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var permissionObserver: PermissionObserver

    @ExperimentalCoroutinesApi
    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        permissionObserver =
            PermissionObserver(
                this, Manifest.permission.ACCESS_FINE_LOCATION, this
            )

        lifecycle.addObserver(permissionObserver)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        adapter = MainAdapter { }
        binding.recyclerView.adapter = adapter

        viewModel.venuesLiveData.observe(this, Observer {
            when (it) {
                VenueResult.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
                is VenueResult.Error -> {
                    binding.progressBar.visibility = View.GONE
                    Snackbar.make(binding.root, it.message, Snackbar.LENGTH_LONG)
                        .setAnchorView(binding.fab).show()
                }
                is VenueResult.Success -> {
                    binding.progressBar.visibility = View.GONE
                    adapter.setItems(it.data.groups[0].items)
                    binding.recyclerView.smoothScrollToPosition(0)
                }
            }
        })

        binding.fab.setOnClickListener {
            permissionObserver.requestPermission()
        }

        binding.searchEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                with(binding.searchEditText) {
                    binding.root.requestFocus()
                    hideKeyboard(this)
                    postDelayed({
                        viewModel.getVenues(text.toString())
                    }, 200)
                }
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }

        binding.root.postDelayed({
            permissionObserver.requestPermission()
        }, 300)
    }

    override fun onPermissionGranted() {
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                location?.let {
                    binding.searchEditText.setText("")
                    viewModel.getVenues(location = LatLng(it.latitude, it.longitude))
                } ?: showLocationError()
            }
    }

    private fun showLocationError() {
        showMessage(R.string.location_error)
    }

    override fun onPermissionDenied() {
        showMessage(R.string.enable_location)
    }

    override fun onPermissionPermanentlyDenied() {
        showMessage(R.string.enable_location_settings)
    }

    private fun showMessage(@StringRes resId: Int) {
        Snackbar.make(binding.root, resId, Snackbar.LENGTH_LONG)
            .setAnchorView(binding.fab).show()
    }

    private fun hideKeyboard(view: View) {
        val inputMethodManager: InputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
}

package com.adyen.android.assignment.repository

import com.adyen.android.assignment.api.PlacesService
import com.adyen.android.assignment.api.VenueRecommendationsQueryBuilder
import com.adyen.android.assignment.api.model.LatLng
import com.adyen.android.assignment.api.model.VenueRecommendationsResponse
import com.adyen.android.assignment.api.model.VenueResult
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class VenueRepositoryImpl @Inject
constructor(
    private val service: PlacesService,
    private val dispatcher: CoroutineDispatcher
) : VenueRepository {

    override fun getVenues(
        location: LatLng?,
        near: String?
    ): Flow<VenueResult<VenueRecommendationsResponse>> {

        val query = VenueRecommendationsQueryBuilder()

        location?.let {
            query.setLatitudeLongitude(location.lat, location.lng)
        }

        near?.let {
            query.setNear(near)
        }

        return flow { emit(service.getVenueRecommendations(query.build())) }
            .map { VenueResult.Success(it.response) }.flowOn(dispatcher)
    }
}
package com.adyen.android.assignment.api

import com.adyen.android.assignment.api.model.ResponseWrapper
import com.adyen.android.assignment.api.model.VenueRecommendationsResponse
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface PlacesService {
    /**
     * Get venue recommendations.
     *
     * See [the docs](https://developer.foursquare.com/docs/api/venues/explore)
     */
    @GET("venues/explore")
    suspend fun getVenueRecommendations(@QueryMap query: Map<String, String>):
            ResponseWrapper<VenueRecommendationsResponse>
}

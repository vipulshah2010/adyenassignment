package com.adyen.android.assignment.repository

import com.adyen.android.assignment.api.model.LatLng
import com.adyen.android.assignment.api.model.VenueRecommendationsResponse
import com.adyen.android.assignment.api.model.VenueResult
import kotlinx.coroutines.flow.Flow

interface VenueRepository {

    fun getVenues(location: LatLng? = null, near: String? = null):
            Flow<VenueResult<VenueRecommendationsResponse>>
}
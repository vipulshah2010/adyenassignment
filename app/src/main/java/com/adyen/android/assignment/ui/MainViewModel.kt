package com.adyen.android.assignment.ui

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.adyen.android.assignment.R
import com.adyen.android.assignment.api.model.LatLng
import com.adyen.android.assignment.api.model.VenueRecommendationsResponse
import com.adyen.android.assignment.api.model.VenueResult
import com.adyen.android.assignment.repository.VenueRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import java.io.IOException
import java.net.SocketTimeoutException

@ExperimentalCoroutinesApi
class MainViewModel @ViewModelInject constructor(
    private val repository: VenueRepository
) : ViewModel() {

    private var _venuesLiveData = MutableLiveData<VenueResult<VenueRecommendationsResponse>>()

    val venuesLiveData: LiveData<VenueResult<VenueRecommendationsResponse>>
        get() = _venuesLiveData

    /**
     *  Fetch venue based on users current location.
     */
    fun getVenues(location: LatLng) {
        viewModelScope.launch {
            repository.getVenues(location)
                .onStart {
                    _venuesLiveData.value = VenueResult.Loading
                }.catch {
                    _venuesLiveData.value = parseError(it)
                }.collect {
                    _venuesLiveData.value = it
                }
        }
    }

    /**
     *  Fetch venue based on users search query.
     */
    fun getVenues(near: String) {
        viewModelScope.launch {
            repository.getVenues(near = near)
                .onStart {
                    _venuesLiveData.value = VenueResult.Loading
                }.catch {
                    _venuesLiveData.value = parseError(it)
                }.collect {
                    _venuesLiveData.value = it
                }
        }
    }

    /**
     * Parse error and return matching error string resource.
     */
    fun parseError(throwable: Throwable) = VenueResult.Error(
        when (throwable) {
            is SocketTimeoutException -> R.string.error_network_timeout
            is IOException -> R.string.error_offline
            else -> R.string.error_search
        }
    )
}
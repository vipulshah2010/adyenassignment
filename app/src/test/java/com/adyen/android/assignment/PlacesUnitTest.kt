package com.adyen.android.assignment

import com.adyen.android.assignment.api.PlacesService
import com.adyen.android.assignment.api.model.LatLng
import com.adyen.android.assignment.api.model.VenueResult
import com.adyen.android.assignment.repository.VenueRepositoryImpl
import com.adyen.android.assignment.utils.CoroutineTest
import com.adyen.android.assignment.utils.InstantTaskExecutorExtension
import com.google.common.truth.Truth
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import okhttp3.OkHttpClient
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

@ExperimentalCoroutinesApi
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(InstantTaskExecutorExtension::class)
internal class PlacesUnitTest : CoroutineTest {

    override lateinit var dispatcher: TestCoroutineDispatcher
    override lateinit var testScope: TestCoroutineScope

    private val placesService: PlacesService by lazy {
        Retrofit.Builder()
            .baseUrl(BuildConfig.FOURSQUARE_BASE_URL)
            .client(OkHttpClient())
            .addConverterFactory(MoshiConverterFactory.create())
            .build().create(PlacesService::class.java)
    }

    /**
     * Currently there is bug with dispatcher.runBlockingTest. it throws error as mentioned here, so using runBlocking.
     * @see <a href="https://github.com/Kotlin/kotlinx.coroutines/issues/1204">Here</a>
     */

    @Test
    fun `valid search returns success`() = runBlocking {
        val repository = VenueRepositoryImpl(placesService, dispatcher)

        repository.getVenues(LatLng(52.376510, 4.905890)).collect {
            Truth.assertThat(it).isInstanceOf(VenueResult.Success::class.java)
        }
    }

    @Test
    fun `invalid search returns 400`() = runBlocking {
        val repository = VenueRepositoryImpl(placesService, dispatcher)

        repository.getVenues(LatLng(0.0, 0.0)).catch {
            Truth.assertThat(it).isInstanceOf(HttpException::class.java)
            Truth.assertThat((it as HttpException).code()).isEqualTo(400)
        }.collect()
    }
}
package com.adyen.android.assignment.utils

import androidx.annotation.VisibleForTesting
import com.adyen.android.assignment.api.model.*

object MockFactory {

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun generateVenues(exception: Boolean) = when {
        exception -> VenueResult.Error(-1)
        else -> VenueResult.Success(
            VenueRecommendationsResponse(
                arrayListOf<VenueRecommendationGroup>().apply {
                    add(VenueRecommendationGroup(arrayListOf(), "First Group", "Hotel"))
                    add(VenueRecommendationGroup(arrayListOf(), "Second Group", "School"))
                },
                "",
                "",
                "",
                SuggestedBounds(LatLng(1.0, 1.0), LatLng(1.0, 1.0)),
                -1,
                -1,
                Warning("")
            )
        )
    }
}
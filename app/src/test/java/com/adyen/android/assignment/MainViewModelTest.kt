package com.adyen.android.assignment

import com.adyen.android.assignment.api.model.VenueResult
import com.adyen.android.assignment.repository.VenueRepository
import com.adyen.android.assignment.ui.MainViewModel
import com.adyen.android.assignment.utils.CoroutineTest
import com.adyen.android.assignment.utils.InstantTaskExecutorExtension
import com.adyen.android.assignment.utils.MockFactory
import com.adyen.android.assignment.utils.observeForTesting
import com.google.common.truth.Truth.assertThat
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import java.io.IOException
import java.net.SocketTimeoutException

@ExperimentalCoroutinesApi
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(InstantTaskExecutorExtension::class)
internal class MainViewModelTest : CoroutineTest {

    override lateinit var dispatcher: TestCoroutineDispatcher
    override lateinit var testScope: TestCoroutineScope

    private val repository: VenueRepository = mockk()

    @Test
    fun `Test loading venues - success`() = dispatcher.runBlockingTest {
        every { repository.getVenues(near = any()) } returns flow {
            emit(MockFactory.generateVenues(exception = false))
        }

        val viewModel = MainViewModel(repository)

        viewModel.venuesLiveData.observeForTesting {
            viewModel.getVenues("Diemen")
            assertThat(it.values).hasSize(2)
            assertThat(it.values[0]).isSameInstanceAs(VenueResult.Loading)
            assertThat(it.values[1]).isInstanceOf(VenueResult.Success::class.java)
            assertThat((it.values[1] as VenueResult.Success).data.groups).hasSize(2)
        }
    }

    @Test
    fun `Test loading venues - returns correct data`() = dispatcher.runBlockingTest {
        every { repository.getVenues(near = any()) } returns flow {
            emit(MockFactory.generateVenues(exception = false))
        }

        val viewModel = MainViewModel(repository)

        viewModel.venuesLiveData.observeForTesting {
            viewModel.getVenues("Diemen")
            assertThat(it.values[1]).isInstanceOf(VenueResult.Success::class.java)
            assertThat((it.values[1] as VenueResult.Success).data.groups).hasSize(2)
        }
    }

    @Test
    fun `Test loading venues - Failure`() = dispatcher.runBlockingTest {
        every { repository.getVenues(near = any()) } returns flow {
            emit(MockFactory.generateVenues(exception = true))
        }

        val viewModel = MainViewModel(repository)

        viewModel.venuesLiveData.observeForTesting {
            viewModel.getVenues("Diemen")
            assertThat(it.values).hasSize(2)
            assertThat(it.values[0]).isSameInstanceAs(VenueResult.Loading)
            assertThat(it.values[1]).isInstanceOf(VenueResult.Error::class.java)
        }
    }

    @Test
    fun `Error is parsed correctly`() {

        val viewModel = MainViewModel(repository)

        assertThat(viewModel.parseError(SocketTimeoutException()).message).isEqualTo(R.string.error_network_timeout)
        assertThat(viewModel.parseError(IOException()).message).isEqualTo(R.string.error_offline)
        assertThat(viewModel.parseError(IllegalAccessException()).message).isEqualTo(R.string.error_search)
    }
}
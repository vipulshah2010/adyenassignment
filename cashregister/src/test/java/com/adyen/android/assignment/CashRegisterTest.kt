package com.adyen.android.assignment

import com.adyen.android.assignment.money.Bill
import com.adyen.android.assignment.money.Change
import com.adyen.android.assignment.money.Coin
import org.junit.Assert
import org.junit.Test

class CashRegisterTest {

    @Test
    fun `Test that Exact payment - Return Nothing`() {
        val itemPrice = Bill.TEN_EURO.minorValue.toLong()
        val amountPaid = Change.none().apply {
            add(Bill.TEN_EURO, 1)
        }

        val change = CashRegister(Change.none()).performTransaction(itemPrice, amountPaid)
        Assert.assertEquals(Change.none(), change)
    }

    @Test
    fun `Test that Lesser Payment - Throw Exception saying under payment`() {
        val itemPrice = Bill.FIFTY_EURO.minorValue.toLong()
        val amountPaid = Change.none().apply {
            add(Bill.TEN_EURO, 1)
        }

        Assert.assertThrows(CashRegister.TransactionException::class.java) {
            CashRegister(Change.none()).performTransaction(itemPrice, amountPaid)
        }
    }

    @Test
    fun `Test that Payment exceeding change amount - Throw Exception saying not enough change available`() {
        val registerChange = Change.none().apply {
            add(Bill.TEN_EURO, 1)
        }
        val itemPrice = Bill.FIFTY_EURO.minorValue.toLong()
        val amountPaid = Change.none().apply {
            add(Bill.ONE_HUNDRED_EURO, 1)
        }

        Assert.assertThrows(CashRegister.TransactionException::class.java) {
            CashRegister(registerChange).performTransaction(itemPrice, amountPaid)
        }
    }

    @Test
    fun `Test that Exact change not available! Throw exception mentioning same`() {
        val registerChange = Change.none().apply {
            add(Bill.TEN_EURO, 1)
        }
        val itemPrice = Bill.FIVE_EURO.minorValue.toLong()
        val amountPaid = Change.none().apply {
            add(Coin.TWO_EURO, 3)
        }

        Assert.assertThrows(CashRegister.TransactionException::class.java) {
            CashRegister(registerChange).performTransaction(itemPrice, amountPaid)
        }
    }

    @Test
    fun `Test that Register returns 1 Euro (Price = 5 Euro, Payment = 6 Euro)`() {
        val registerChange = Change.none().apply {
            add(Bill.TEN_EURO, 1)
            add(Coin.TWO_CENT, 2)
            add(Coin.ONE_EURO, 2)
        }

        val itemPrice = Bill.FIVE_EURO.minorValue.toLong()
        val amountPaid = Change.none().apply {
            add(Coin.TWO_EURO, 3)
        }

        val change = CashRegister(registerChange).performTransaction(itemPrice, amountPaid)
        Assert.assertEquals(Change.none().apply {
            add(Coin.ONE_EURO, 1)
        }, change)

        // Assert that cash register left with 10 Euro = 1 count, 2 Euro = 2 counts, 1 euro = 1 count)
        Assert.assertEquals(Change.none().apply {
            add(Bill.TEN_EURO, 1)
            add(Coin.TWO_CENT, 2)
            add(Coin.ONE_EURO, 1)
        }, registerChange)
    }

    @Test
    fun `Test that Register returns 1 Cent (Price = 5 euro, Payment = 5 Euro 1 Cent)`() {
        val registerChange = Change.none().apply {
            add(Bill.FIVE_EURO, 1)
            add(Coin.TWO_CENT, 2)
            add(Coin.ONE_CENT, 1)
        }

        val itemPrice = Bill.FIVE_EURO.minorValue.toLong()
        val amountPaid = Change.none().apply {
            add(Coin.TWO_EURO, 2)
            add(Coin.TEN_CENT, 8)
            add(Coin.FIVE_CENT, 3)
            add(Coin.TWO_CENT, 3)
            // 5 Euro 1 Cent
        }

        val change = CashRegister(registerChange).performTransaction(itemPrice, amountPaid)
        Assert.assertEquals(Change.none().apply {
            add(Coin.ONE_CENT, 1)
        }, change)
    }
}

package com.adyen.android.assignment

import com.adyen.android.assignment.money.Bill
import com.adyen.android.assignment.money.Change
import com.adyen.android.assignment.money.Coin
import com.adyen.android.assignment.money.MonetaryElement

/**
 * The CashRegister class holds the logic for performing transactions.
 *
 * @param change The change that the CashRegister is holding.
 */
class CashRegister(private val change: Change) {
    /**
     * Performs a transaction for a product/products with a certain price and a given amount.
     *
     * @param price The price of the product(s).
     * @param amountPaid The amount paid by the shopper.
     *
     * @return The change for the transaction.
     *
     * @throws TransactionException If the transaction cannot be performed.
     */
    fun performTransaction(price: Long, amountPaid: Change): Change {
        var returnAmount = amountPaid.total - price
        return when {
            // Exact payment - Return Nothing
            returnAmount == 0L -> Change.none()

            // Lesser Payment - Throw Exception saying under payment.
            returnAmount < 0 -> throw TransactionException("Payment amount is less than product price.")

            // Payment exceeding change amount - Throw Exception saying not enough change available
            (returnAmount > change.total) -> throw TransactionException("Sorry! we don't have enough change.")
            else -> {
                val monetaryElements: MutableList<MonetaryElement> = Bill.values().toMutableList()
                monetaryElements.addAll(Coin.values())

                val returnChange = Change.none()

                monetaryElements.forEach {
                    val neededCount = returnAmount / it.minorValue
                    val availableCount = change.getCount(it)
                    if (neededCount > 0 && availableCount > 0) {
                        if (neededCount >= availableCount) {
                            returnChange.add(it, availableCount)
                        } else {
                            returnChange.add(it, neededCount.toInt())
                        }
                        // Deduct count from cash register
                        change.remove(it, returnChange.getCount(it))

                        // update return amount
                        returnAmount -= (returnChange.getCount(it) * it.minorValue)
                    }
                    if (returnAmount == 0L) {
                        return returnChange
                    }
                }

                // Exact change not available! Throw exception mentioning same.
                throw TransactionException("Sorry! We don't have exact change.")
            }
        }
    }

    class TransactionException(message: String, cause: Throwable? = null) :
        Exception(message, cause)
}
